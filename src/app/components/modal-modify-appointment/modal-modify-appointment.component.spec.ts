import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModifyAppointmentComponent } from './modal-modify-appointment.component';

describe('ModalModifyAppointmentComponent', () => {
  let component: ModalModifyAppointmentComponent;
  let fixture: ComponentFixture<ModalModifyAppointmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalModifyAppointmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalModifyAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
