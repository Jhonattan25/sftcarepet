import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2';
import { interval } from 'rxjs';


@Component({
  selector: 'app-modal-modify-appointment',
  templateUrl: './modal-modify-appointment.component.html',
  styleUrls: ['./modal-modify-appointment.component.css']
})
export class ModalModifyAppointmentComponent implements OnInit {
  form!: FormGroup;
  pets!: Array<any>;
  element = this.data.content;


  constructor(private client: ClientService, private fb: FormBuilder, public dialog: MatDialogRef<ModalModifyAppointmentComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.consultPets();
    this.form = this.fb.group({
      date: ['', [Validators.required]],
      hour: ['', [Validators.required, Validators.minLength(1)]],
      pet_id: ['', [Validators.required, Validators.min(1)]],
    });
    this.initValuesForm();
  }

  consultPets() {
    this.client.getRequestConsultPets("http://localhost:10101/pet/consult").subscribe(
      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.pets = response.pets;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        //se imprime el status del error
        console.log(error.status);
      }
    )
  }

  private initValuesForm(): void {
    console.log(this.element);
    
    this.form.patchValue({
      date: this.element.date,
      hour: this.element.hour,
      pet_id: this.element.pet_id,
    }); 
  }

  onSubmit() {
    if (this.form.valid) {
      this.client.updateRequestUpdateAppointment(`http://localhost:10101/appointment/update/?id=${this.element.id}`, {
        date: this.form.value.date,
        hour: this.form.value.hour,
        state: this.element.state,
        pet_id: this.form.value.pet_id,
      }).subscribe(
        (reponse: any) => {
          console.log(reponse);
          this.element.date = this.form.value.date;
          this.element.hour = this.form.value.hour;
          this.element.pet_id = this.form.value.pet_id;
          for (const iterator of this.pets) {
            if (iterator.id === this.element.pet_id) {
              this.element.name = iterator.name;
              break;
            }
          }
          this.dialog.close(true);
        },
        (error) => {
          console.log(error.status);
        }
      )
    } else {
      console.log("form error");
    }
  }

  cancelModify() {
    this.dialog.close(true);
  }

}
