import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SecurityService } from '../../services/security.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  login: boolean = localStorage.getItem('token') ? true : false;

  constructor(private router: Router, private dialog: MatDialog, private security: SecurityService) { }

  public app_name = "CarePet";

  ngOnInit(): void {
    // this.verifyLogin();
  }

  // verifyLogin() {
  //   this.security.verifyLogin().subscribe(
  //     (response) => this.login = response, (err) => console.log(err)
  //   );
  // }

  // showLogin() {
  //   this.dialog.open(LoginComponent);
  // }

  // showSignOff(){
  //   localStorage.removeItem('token');
  //   this.router.navigate(['/']);
  // }
}
