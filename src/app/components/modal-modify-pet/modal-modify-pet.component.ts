import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-modify-pet',
  templateUrl: './modal-modify-pet.component.html',
  styleUrls: ['./modal-modify-pet.component.css']
})
export class ModalModifyPetComponent implements OnInit {
  form!: FormGroup;
  element = this.data.content;

  constructor(private client: ClientService, private fb: FormBuilder, public dialog: MatDialogRef<ModalModifyPetComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      race: ['', [Validators.required, Validators.min(1), Validators.maxLength(30)]],
      weight: ['', [Validators.required, Validators.min(1), Validators.max(80)]],
      birthdate: ['', [Validators.required]],
      type_pet: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    });
    this.initValuesForm();
  }

  private initValuesForm(): void {
    this.form.patchValue({
      name: this.element.name,
      race: this.element.race,
      weight: this.element.weight,
      birthdate: this.element.birthdate,
      type_pet: this.element.type_pet
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.client.updateRequestUpdatePet(`http://localhost:10101/pet/update/?id=${this.element.id}`, {
        name: this.form.value.name,
        race: this.form.value.race,
        weight: this.form.value.weight,
        birthdate: this.form.value.birthdate,
        type_pet: this.form.value.type_pet
      }).subscribe(
        (reponse: any) => {
          console.log(reponse);
          this.element.name = this.form.value.name;
          this.element.race = this.form.value.race;
          this.element.weight = this.form.value.weight;
          this.element.birthdate = this.form.value.birthdate;
          this.element.type_pet = this.form.value.type_pet;
          this.dialog.close(true);

          Swal.fire({
            icon: 'success',
            title: 'Mascota modificada con exito',
            background: '#fff',
            confirmButtonColor: '#045b62'
          });
        },
        (error) => {
          console.log(error.status);
        }
      )
    } else {
      console.log("form error");
    }
  }

  cancelModify() {
    this.dialog.close(true);
  }

}
