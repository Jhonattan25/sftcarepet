import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModifyPetComponent } from './modal-modify-pet.component';

describe('ModalModifyPetComponent', () => {
  let component: ModalModifyPetComponent;
  let fixture: ComponentFixture<ModalModifyPetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalModifyPetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalModifyPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
