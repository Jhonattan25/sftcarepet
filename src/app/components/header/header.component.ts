import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { MatDialog } from '@angular/material/dialog';
import { SecurityService } from '../../services/security.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  login: boolean = localStorage.getItem('token') ? true : false;

  constructor(private router: Router, private dialog: MatDialog, private security: SecurityService) { }

  public app_name = "CarePet";

  ngOnInit(): void {
    this.verifyLogin();
  }

  verifyLogin() {
    this.security.verifyLogin().subscribe(
      (response:any) => this.login = response, (err) => console.log(err)
    );
  }

  showLogin() {
    this.dialog.open(LoginComponent);
  }

  showSignOff(){
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }
}
