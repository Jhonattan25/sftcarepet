import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  //metodo que recibe como parametro una url un json a ser enviado. Esta solicitud se hace con metodo POST
  //en este caso el json proviene de los datos de un formulario.
  postRequestRegisterUser(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }

    const header = new HttpHeaders().set('Authorization', '57ydf544ljka559ahjkfgd1');
    config["header"] = header;
    //Notese que como tercer parametro se pasa la configuracion de la request
    return this.http.post(route, data, config);
  }

  postRequestRegisterPet(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.post(route, data, config);
  }

  postRequestRegisterAppointment(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.post(route, data, config);
  }

  updateRequestUpdateAppointment(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.put(route, data, config);
  }

  postRequestRegisterHistory(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.post(route, data, config);
  }

  postRequestLogin(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', '57ydf544ljka559ahjkfgd1');
    config["headers"] = header;

    return this.http.post(route, data, config);
  }

  getRequestConsultPets(route: string){
    //configuracion del tipo de respuesta esperado
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    //Notese que como tercer parametro se pasa la configuracion de la request
    return this.http.get(route, config);
  }

  getRequestConsultAppointment(route: string){
    //configuracion del tipo de respuesta esperado
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    //Notese que como tercer parametro se pasa la configuracion de la request
    return this.http.get(route, config);
  }

  updateRequestUpdatePet(route: string, data: any) {
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    return this.http.put(route, data, config);
  }
  
  getRequestConsultMedicalHistory(route: string){
    //configuracion del tipo de respuesta esperado
    let config: any = {
      responseType: "json"
    }
    const header = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
    config["headers"] = header;

    //Notese que como tercer parametro se pasa la configuracion de la request
    return this.http.get(route, config);
  }
}