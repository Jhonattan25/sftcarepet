import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { RegisterUserComponent } from './pages/register-user/register-user.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterPetComponent } from './pages/register-pet/register-pet.component';

import { RegisterAppointmentComponent } from './pages/register-appointment/register-appointment.component';
import { RegisterClinicalHistoryComponent } from './pages/register-clinical-history/register-clinical-history.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableConsultPetComponent } from './pages/table-consult-pet/table-consult-pet.component';
import { ModalModifyPetComponent } from './components/modal-modify-pet/modal-modify-pet.component';
import { TableConsultMedicalHistoryComponent } from './pages/table-consult-medical-history/table-consult-medical-history.component';
import { ConsultAppointmentsComponent } from './pages/consult-appointments/consult-appointments.component';
import { ModalModifyAppointmentComponent } from './components/modal-modify-appointment/modal-modify-appointment.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterUserComponent,
    HeaderComponent,
    FooterComponent,
    RegisterPetComponent,
    RegisterAppointmentComponent,
    RegisterClinicalHistoryComponent,
    LoginComponent,
    TableConsultPetComponent,
    ModalModifyPetComponent,
    TableConsultMedicalHistoryComponent,
    ConsultAppointmentsComponent,
    ModalModifyAppointmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
