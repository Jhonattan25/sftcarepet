import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { RegisterUserComponent } from './pages/register-user/register-user.component';
import { RegisterPetComponent } from './pages/register-pet/register-pet.component';
import { TableConsultPetComponent } from './pages/table-consult-pet/table-consult-pet.component';
import { RegisterAppointmentComponent } from './pages/register-appointment/register-appointment.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterClinicalHistoryComponent } from './pages/register-clinical-history/register-clinical-history.component';
import { TableConsultMedicalHistoryComponent } from './pages/table-consult-medical-history/table-consult-medical-history.component';
import { ConsultAppointmentsComponent } from './pages/consult-appointments/consult-appointments.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'user/register', component: RegisterUserComponent},
  {path: 'pet/register', component: RegisterPetComponent},
  {path: 'pet/consult', component: TableConsultPetComponent},
  {path: 'appointment/register', component: RegisterAppointmentComponent},
  {path: 'clinical/history', component: RegisterClinicalHistoryComponent},
  {path: 'medicalHistory/consult', component: TableConsultMedicalHistoryComponent},
  {path: 'appointment/consult', component: ConsultAppointmentsComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
