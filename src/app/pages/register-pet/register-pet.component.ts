import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-pet',
  templateUrl: './register-pet.component.html',
  styleUrls: ['./register-pet.component.css']
})
export class RegisterPetComponent implements OnInit {
  form!: FormGroup;

  constructor(private client: ClientService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(1),
      Validators.maxLength(100)]],

      race: ['', [Validators.required, Validators.min(1),
      Validators.maxLength(30)]],

      weight: ['', [Validators.required, Validators.min(1), Validators.max(80)]],

      birthdate: ['', [Validators.required]],

      type_pet: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    });
  }
  onSubmit() {
    if (this.form.valid) {
      this.client.postRequestRegisterPet('http://localhost:10101/pet/register', {
        name: this.form.value.name,
        race: this.form.value.race,
        weight: this.form.value.weight,
        birthdate: this.form.value.birthdate,
        type_pet: this.form.value.type_pet
      }).subscribe(
        (reponse: any) => {
          console.log(reponse);
          Swal.fire({
            icon: 'success',
            title: 'Registro exitoso',
            background: '#fff',
            confirmButtonColor: '#0d6efd'
          });
          this.router.navigate(['/pet/consult']);
        },
        (error) => {
          console.log(error.status);
          
        }
      )
    } else {
      console.log("form error");
      Swal.fire({
        icon: 'warning',
        title: 'Error, llenar todos lo campos',
        background: '#fff',
        confirmButtonColor: '#0d6efd'
      });
    }
  }
}
