import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterClinicalHistoryComponent } from './register-clinical-history.component';

describe('RegisterClinicalHistoryComponent', () => {
  let component: RegisterClinicalHistoryComponent;
  let fixture: ComponentFixture<RegisterClinicalHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterClinicalHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterClinicalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
