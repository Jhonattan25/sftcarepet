import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-clinical-history',
  templateUrl: './register-clinical-history.component.html',
  styleUrls: ['./register-clinical-history.component.css']
})
export class RegisterClinicalHistoryComponent implements OnInit {
  form!: FormGroup;
  pets!: Array<any>;
  seleccionados:string[]=[];
  description!:String;
  constructor(private client: ClientService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.consultPets();
    this.form = this.fb.group({
      date: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.min(1)]],
      name_vet: ['', [Validators.required, Validators.min(1)]],
      pet_id: ['', [Validators.required, Validators.min(1)]],
    });
  }

  consultPets() {
    this.client.getRequestConsultPets("http://localhost:10101/pet/consult").subscribe(

      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.pets = response.pets;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        this.router.navigate(['/']);
        //se imprime el status del error
        console.log(error.status);
      }
    )
  }

  onSubmit() {
    console.log("tet");
    
    if (this.form.valid) {
      this.client.postRequestRegisterHistory('http://localhost:10101/medicalHistory/register',{
        date: this.form.value.date,
        description: this.form.value.description,
        pet_id: this.form.value.pet_id,
        name_vet: this.form.value.name_vet,
      }).subscribe(
        (reponse: any)=> {
          console.log(reponse);
        },
        (error)=>{
          console.log(error.status);
        }
      )
      console.log("entre");

    } else {
      console.log("form error");
    }
  }
}
