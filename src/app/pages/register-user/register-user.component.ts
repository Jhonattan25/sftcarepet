import { Component, OnInit } from '@angular/core';
// Importacion de servicios
import { ClientService } from '../../client.service';
// Importacion de clases necesarias para manejar formularios reactivos y el routing
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// Para las ventana modales
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  hide = true;
  //grupo de controles de nuestro formulario
  form!: FormGroup;

  //inyeccion de dependencias
  constructor(private client: ClientService, private fb: FormBuilder, private router: Router) { }

  //en ngOnInit() metemos todas las instrucciones que queremos que se ejecuten apenas se cree nuestro componente
  ngOnInit(): void {

    this.form = this.fb.group({
      id: ['', [Validators.required, Validators.min(10000000), Validators.max(9999999999)]],
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      lastname: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      address: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
      email: ['', [Validators.required, Validators.email]],
      cellphone: ['', [Validators.required, Validators.min(3000000000), Validators.max(3999999999)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15)]]
    });
  }

  onSubmit() {
    // Si la validación del formulario es exitosa...
    if (this.form.valid) {
      //se envian los datos del formulario mediante una solicitud POST, los valores de los inputs del formulario
      this.client.postRequestRegisterUser('http://localhost:10101/user/register', {
        id: this.form.value.id,
        name: this.form.value.name,
        lastname: this.form.value.lastname,
        address: this.form.value.address,
        email: this.form.value.email,
        cellphone: this.form.value.cellphone,
        type_role: 'Cliente',
        password: this.form.value.password
      }).subscribe(
        (response: any) => {
          console.log(response);

          Swal.fire({
            icon: 'success',
            title: 'Registro exitoso',
            background: '#fff',
            confirmButtonColor: '#0d6efd'
          });

          this.router.navigate(['/']);
        },
        (error) => {
          console.log(error.status);
          
        }
      );
    } else {
      console.log('Form error');
      Swal.fire({
        icon: 'warning',
        title: 'Error, llenar todos lo campos',
        background: '#fff',
        confirmButtonColor: '#0d6efd'
      });
    }
  }
}
