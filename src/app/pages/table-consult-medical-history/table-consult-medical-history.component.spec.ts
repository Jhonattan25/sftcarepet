import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableConsultMedicalHistoryComponent } from './table-consult-medical-history.component';

describe('TableConsultMedicalHistoryComponent', () => {
  let component: TableConsultMedicalHistoryComponent;
  let fixture: ComponentFixture<TableConsultMedicalHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableConsultMedicalHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableConsultMedicalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
