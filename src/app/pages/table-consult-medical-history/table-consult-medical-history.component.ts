import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-table-consult-medical-history',
  templateUrl: './table-consult-medical-history.component.html',
  styleUrls: ['./table-consult-medical-history.component.css']
})
export class TableConsultMedicalHistoryComponent implements OnInit {

  constructor(private client: ClientService, private router: Router) { }

  medicalHistories!: Array<any>;

  ngOnInit(): void {
    this.consultMedicalHistory();
  }

  consultMedicalHistory() {
    this.client.getRequestConsultMedicalHistory("http://localhost:10101/medicalHistory/consult").subscribe(

      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.medicalHistories = response.medicalHistory;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        this.router.navigate(['/']);
        //se imprime el status del error
        console.log(error.status);
      }
    )
  }
}
