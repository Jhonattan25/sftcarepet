import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-appointment',
  templateUrl: './register-appointment.component.html',
  styleUrls: ['./register-appointment.component.css']
})
export class RegisterAppointmentComponent implements OnInit {
  form!: FormGroup;
  pets!: Array<any>;

  constructor(private client: ClientService, private fb:FormBuilder, private router: Router) { }
  
  ngOnInit(): void {
    this.consultPets();
    this.form = this.fb.group({
      date:['', [Validators.required]],

      hour:['', [Validators.required, Validators.minLength(1)]],

      pet_id: ['',[Validators.required, Validators.min(1) ]],
    });    
  }
  consultPets() {
    this.client.getRequestConsultPets("http://localhost:10101/pet/consult").subscribe(

      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.pets = response.pets;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        this.router.navigate(['/']);
        //se imprime el status del error
        console.log(error.status);
      }
    )
  }
  onSubmit() {
    if(this.form.valid){
      this.client.postRequestRegisterAppointment('http://localhost:10101/appointment/register',{
        date: this.form.value.date,
        hour: this.form.value.hour,
        state: "Asignada",
        pet_id: this.form.value.pet_id,
                
      }).subscribe(
        (reponse: any)=> {
          console.log(reponse);
          Swal.fire({
            icon: 'success',
            title: 'Cita asignada con exito',
            background: '#fff',
            confirmButtonColor: '#0d6efd'
          });
          this.router.navigate(['/appointment/consult']);
        },
        (error)=>{
          console.log(error.status);
        }
      )
    }else{
      console.log("form error");      
    }
  }
}
