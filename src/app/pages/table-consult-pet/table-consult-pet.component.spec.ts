import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableConsultPetComponent } from './table-consult-pet.component';

describe('TableConsultPetComponent', () => {
  let component: TableConsultPetComponent;
  let fixture: ComponentFixture<TableConsultPetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableConsultPetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableConsultPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
