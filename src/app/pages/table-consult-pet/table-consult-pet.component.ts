import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalModifyPetComponent } from '../../components/modal-modify-pet/modal-modify-pet.component';

@Component({
  selector: 'app-table-consult-pet',
  templateUrl: './table-consult-pet.component.html',
  styleUrls: ['./table-consult-pet.component.css']
})
export class TableConsultPetComponent implements OnInit {

  pets!: Array<any>;

  constructor(private client: ClientService, private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.consultPets();
  }

  consultPets() {
    this.client.getRequestConsultPets("http://localhost:10101/pet/consult").subscribe(

      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        this.pets = response.pets;
        console.log(response);
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        this.router.navigate(['/']);
        //se imprime el status del error
        console.log(error.status);
      }
    )
  }

  onEditPet(element: any) {
    this.openDialog(element);
  }

  openDialog(element: any): void {
    const config = {
      data: {
        message: element ? 'Editar reporte' : 'Error',
        content: element
      }
    };
    const dialogRef = this.dialog.open(ModalModifyPetComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Result ${result}`);
    });
  }

  onDeletePet(element: any) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea eliminar este reporte?',
      showCancelButton: true,
      cancelButtonText: `Cancelar`,
      showConfirmButton: true,
      confirmButtonText: `Eliminar`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      //Read more about isConfirmed, isDenied below
      if (result.isConfirmed) {
        console.log('Mascota eliminada');
        for (const key in this.pets) {
          if (this.pets[key].id === element.id) {
            this.pets.splice(parseInt(key), 1);
            break;
          }
        }
      } else if (result.isDenied) {
        Swal.fire('La mascota no se ha eliminado', '', 'info')
      }
    });
  }
}
