import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultAppointmentsComponent } from './consult-appointments.component';

describe('ConsultAppointmentsComponent', () => {
  let component: ConsultAppointmentsComponent;
  let fixture: ComponentFixture<ConsultAppointmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultAppointmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultAppointmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
