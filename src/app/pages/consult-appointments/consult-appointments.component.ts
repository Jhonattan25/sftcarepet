import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from '../../client.service';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalModifyAppointmentComponent } from '../../components/modal-modify-appointment/modal-modify-appointment.component';


@Component({
  selector: 'app-consult-appointments',
  templateUrl: './consult-appointments.component.html',
  styleUrls: ['./consult-appointments.component.css']
})
export class ConsultAppointmentsComponent implements OnInit {

  medicals!: Array<any>;

  constructor(private client: ClientService, private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.consultAppointments();
  }

  consultAppointments() {
    this.client.getRequestConsultAppointment("http://localhost:10101/appointment/consult").subscribe(

      //cuando la respuesta del server llega es emitida por el observable mediante next()..
      (response: any) => {
        console.log(response);
        console.log(response.medical);
        this.medicals = response.medical;
        
      },
      //si ocurre un error en el proceso de envío del formulario...
      (error) => {
        this.router.navigate(['/']);
        //se imprime el status del error
        console.log(error.status);
      }
    )
  }

  onEditAppointment(element: any) {
    this.openDialog(element);
  }

  openDialog(element: any): void {
    const config = {
      data: {
        message: element ? 'Editar reporte' : 'Error',
        content: element
      }
    };
    const dialogRef = this.dialog.open(ModalModifyAppointmentComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Result ${result}`);
    });
  }

  onCancelAppointment(element: any) {
    Swal.fire({
      icon: 'question',
      title: '¿Desea cancelar esta cita?',
      showCancelButton: true,
      cancelButtonText: `No cancelar cita`,
      showConfirmButton: true,
      confirmButtonText: `Cancelar cita`,
      confirmButtonColor: '#488D95'
    }).then((result) => {
      //Read more about isConfirmed, isDenied below
      if (result.isConfirmed) {
        this.client.updateRequestUpdateAppointment(`http://localhost:10101/appointment/update/?id=${element.id}`, {
        date: element.date,
        hour: element.hour,
        state: 'Cancelada',
        pet_id: element.pet_id,
      }).subscribe(
        (reponse: any) => {
          console.log(reponse);
          element.state = 'Cancelada';
        },
        (error) => {
          console.log(error.status);
        }
      )
      } else if (result.isDenied) {
        Swal.fire('La cita no se ha cancelado', '', 'info')
      }
    });
  }
}
